package code2021

import java.io.File

fun main(){
    val input = File("src/main/Input/input2021/day2").readLines()
    var posX = 0
    var posY = 0
    input.forEachIndexed { index, pos ->
        val position = pos.split(" ")
//        println(position)
        when(position[0]){
            "up"-> posY -= position[1].toInt()
            "down"-> posY += position[1].toInt()
            else -> posX += position[1].toInt()
        }
    }
    println(posX * posY)
}