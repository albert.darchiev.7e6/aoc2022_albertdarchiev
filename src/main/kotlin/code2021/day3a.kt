package code2021

import java.io.File

fun main(){
    val input = File("src/main/Input/input2021/day3").readLines()
    var counterOne = 0
    val epsilon = mutableListOf<Char>()
    val gamma = mutableListOf<Char>()

    var position = 0
    repeat(input[0].length) {
        for(i in input){
            if (i[position].toString() == "1") counterOne++
            println(i[position])
        }

        println("------- "+counterOne)
        position ++
        if (counterOne > input.size/2) {
            epsilon.add('1')
            gamma.add('0')
        }
        else {
            epsilon.add('0')
            gamma.add('1')
        }
        counterOne = 0
    }
    println(epsilon)
    println(gamma)

    //RESULT
    println( binaryToDecimal(epsilon) * binaryToDecimal(gamma))
}

fun binaryToDecimal(bin: MutableList<Char>): Int {
    var decimal = 0
    var base = 1
    for (i in bin.size-1 downTo 0) {
        if (bin[i] == '1') {
            decimal += base
        }
        base *= 2
    }

    return decimal
}