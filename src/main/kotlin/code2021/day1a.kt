package code2021

import java.io.File

fun main(){
    val input = File("src/main/Input/input2021/day1").readLines()
    var counter = 0
    var lastNum = 0
    input.forEachIndexed { index, num->
        if(num.toInt() > lastNum && index != 0) counter ++
        lastNum = num.toInt()
    }
    println(counter)
}