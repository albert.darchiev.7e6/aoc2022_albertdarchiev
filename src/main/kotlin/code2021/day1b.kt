package code2021

import java.io.File

fun main(){
    val input = File("src/main/Input/input2021/day1").readLines()
    var counter = 0
    var lastNum = 0
    println(input.size)
    input.forEachIndexed { i, num->
        if (i < input.size-2) {
            val numSum = input[i].toInt() + input[i + 1].toInt() + input[i + 2].toInt()
            if (numSum > lastNum && i != 0) counter++
            lastNum = numSum
        }
    }
    println(counter)
}