package code2022
fun main() {
    val input = readFile("day2")
    println(input)

    var oponent = 0
    var user = 0

    //OPTION 1
    // "A" = "ROCK"
    // "B" = "PAPER"
    // "C" = "SCISORS"

    //OPTION 2
    // X = "ROCK"
    // Y = "PAPER"
    // Z = "SCISORS"
var option1 = " "
    do {

        for (i in input){
            val split = i.split(" ")
            option1 = split[0]
            var option2 = split[1]
            when(option2){
                "X" -> if (option1 == "A") option2 = "Z"
                        else if (option1 == "B") option2 = "X"
                        else option2 = "Y"
                "Y" -> if (option1 == "A") option2 = "X"
                else if (option1 == "B") option2 = "Y"
                else option2 = "Z"
                "Z" -> if (option1 == "A") option2 = "Y"
                        else if (option1 == "B") option2 = "Z"
                        else option2 = "X"
            }
            when(option1){
                "A" -> if (option2 == "X") user += 1+3
                        else if (option2 == "Y") user += 2+6
                        else user += 3
                "B" -> if (option2 == "X") user += 1
                        else if (option2 == "Y") user += 2+3
                        else user += 3+6
                "C" -> if (option2 == "X") user += 1+6
                        else if (option2 == "Y") user += 2
                        else user += 3+3
            }

        }
        println(user)
    }while (option1 == "")
}