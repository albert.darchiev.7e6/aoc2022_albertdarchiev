package code2022

fun main() {
    val fileName = "day5"
    val input = readFile(fileName)
    val letters: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf("G","W","L","J","B","R","T","D"),
        mutableListOf("C", "W", "S"),
        mutableListOf("M", "T", "Z", "R"),
        mutableListOf("V", "P", "S", "H", "C", "T", "D"),
        mutableListOf("Z", "D", "L", "T", "P", "G"),
        mutableListOf("D", "C", "Q", "J", "Z", "R", "B", "F"),
        mutableListOf("R", "T", "F", "M", "J", "D", "B", "S"),
        mutableListOf("M", "V", "T", "B", "R", "H", "L"),
        mutableListOf("V", "S", "D", "P", "Q")
    )

    println()
    for (i in input.indices){
        val move = input[i].split(" ")[1].toInt()
        val from = input[i].split(" ")[3].toInt()
        val to = input[i].split(" ")[5].toInt()
        print("move $move from $from to $to")
        println()

        println("OLD:")
        for (elem in letters) {
            print("${letters.indexOf(elem)+1} ")
            println(elem)
        }

        for (l in 0 until move){
            // move = 5 and l = 4 error
            val let = letters[from-1].first()
            letters[to-1].add(0, let) //ADD TO X
            letters[from-1].remove(let) //REMOVE LETTER FROM X
        }

        println("NEW:")
        for (elem in letters) {
            print("${letters.indexOf(elem)+1} ")
            println(elem)
        }
        println()
    }
    println()
    for (elem in letters) print(elem.first())
}