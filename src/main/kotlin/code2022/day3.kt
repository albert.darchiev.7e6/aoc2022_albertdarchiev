package code2022

import java.util.StringJoiner
import kotlin.text.Typography.half

/*
fun main() {
    val fileName = "day3"
    val input = readFile(fileName)
    println(input)
    var part1 = listOf<Char>()
    var part2 = listOf<Char>()
    var counter = 0
    //A    B	C	D	E	F	G	H	I	J	K	L	M	N	O	P	Q	R	S	T	U	V	W	X	Y	Z
    //27   28   29  30  31  32  33  34  35  36  37  38  39  40  41  42  43  44  45  46  47  48  49  50  51  52

    //a	   b	c	d	e	f	g	h	i	j	k	l	m	n	o	p	q	r	s	t	u	v	w	x	y	z
    //1    2    3   4   5   6   7   8   9   10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26
    for (group in 0 until input.size) {
            for (l in input[group][0]) { //24 32
                part1 += l
            }
            for (l in input[group][1]) {
                part2 += l
            }
            for (i in 0 until input[group][2].length){
                if (part1.contains(input[group][2][i]) && part2.contains(input[group][2][i]) && (input[group][2][i]).toInt() in 97..122 ){
                    counter += (input[group][2][i]).toInt() - 96
                    break
                }
                else if (part1.contains(input[group][2][i]) && part2.contains(input[group][2][i]) && (input[group][2][i]).toInt() in 65..90 ){
                    counter += (input[group][2][i]).toInt() - 38
                    break
                }
            }
            println(part1)
            println(part2)
            part1 = listOf<Char>()
            part2 = listOf<Char>()


        println(part1)
        println(part2)
        println(counter)
    }


}


 */


/*

for (group in 0 until input.size) {
        for (word in 0 until input[group].size) {
            val half = input[group][word].length / 2

            for (l in 0 until half) {
                part1 += input[group][word][l]
            }
            for (l in half until input[group][word].length) {
                part2 += input[group][word][l]
            }

            for (i in part1) {
                if (part2.contains(i) && i.toInt() in 97..122) {
                    counter += i.toInt() - 96
                    break
                } else if (part2.contains(i) && i.toInt() in 65..90) {
                    counter += i.toInt() - 38
                    break
                }
            }
            println(part1)
            println(part2)
            part1 = listOf<Char>()
            part2 = listOf<Char>()
        }*/
