package code2022

fun main() {
    fun part1(input: List<String>): Int {
        println(input)
        var counter = 0
        var top1 = 0
        var top2 = 0
        var top3 = 0
        var result = 0


        for (num in input) {
            if (num.length > 0) counter += num.toInt()
            else {
                if (counter > top1) top1 = counter
                counter = 0
            }
        }
        println(top1)

        for (i in input) {
            if (i.length > 0 ) counter += i.toInt()
            else {
                if (counter > top2 && counter != top1 && counter != 96833) top2 = counter
                counter = 0
            }
        }
        println(top2)

        for (i in input) {
            if (i.length > 0) counter += i.toInt()
            else {
                if (counter > top3 && counter != top2 && counter != top1 && counter != 96833) top3 = counter
                counter = 0
            }
        }
        println(top3)

        result = top1+top2+top3
        println("RESULT "+ result)
        println(top1)
        return result

    }
    val input = readFile("day1")
    println(part1(input))
}
// 50030