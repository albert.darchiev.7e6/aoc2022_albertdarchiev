package code2022

import java.io.File

fun readFile(fileName: String): List<String> {
    return File("src/main/Input/$fileName").readLines()
    //    return File("src/main/Files/$fileName").readLines().toList().chunked(3) - DAY3
}